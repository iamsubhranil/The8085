#pragma once

#include "common.h"

const char* machine_get_opcode_string(u8 opcode);
void mem_write(u16 add, u8 val);
void machine_init();
void machine_print();
void machine_run();
