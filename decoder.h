#pragma once

#include "common.h"

typedef enum{
    #define INSTRUCTION(name , x, y) \
        name,
    #include "instruction.h"
    #undef INSTRUCTION
} OpCode;

typedef struct{
    OpCode op;
    // Types of operands
    // 1 for reg, 0 for mem
    // mem address will be implied in the
    // bytecode after the opcode
    u8 operand0, operand1;
    u8 reg[2];
} Instruction;

Instruction decode_ins(u8 *ins);
