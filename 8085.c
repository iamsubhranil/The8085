#include <stdio.h>

#include "8085.h"
#include "decoder.h"
#include "display.h"

typedef union{
    u8 val8[2];
    u16 val16;
} RegPair;

typedef struct{
   /* union{
        u8 accumulator;
        i8 accumulatori;
    }; */
    union{
        u8 tmp;
        i8 tmpi;
    };

    // reg[6] is accumulator
    // reg[7] is psw
    // PSW bitmap :  
    //  D7 	D6 	D5 	D4 	D3 	D2 	D1 	D0
    //  S 	Z 		AC 		P 		CY
    //
    //  S -> Sign
    //  Z -> Zero
    //  AC -> Auxiliary Carry
    //  P -> Parity
    //  C -> Carry
    union{
        u8 reg[8];
        i8 regi[8];
    };
    u16 pc, sp;
   // u8 flag;
} Machine;

static Machine m;
static u8 mem[1 << 15];

static void machine_set_flag(char c, u8 val){
    //dbg("Setting %c to 0x%x\t-val : 0x%x\t1 << 7 : 0x%x", c, val, -val, 1 << 7);
    //dbg("Before : 0x%x", m.reg[7]);
    switch(c){
        case 'S':
            m.reg[7] ^= (-val ^ m.reg[7]) & (1UL << 7);
            break;
        case 'Z':
            m.reg[7] ^= (-val ^ m.reg[7]) & (1UL << 6);
            break;
        case 'A':
            m.reg[7] ^= (-val ^ m.reg[7]) & (1UL << 4);
            break;
        case 'P':
            m.reg[7] ^= (-val ^ m.reg[7]) & (1UL << 2);
            break;
        case 'C':
            m.reg[7] ^= (-val ^ m.reg[7]) & 1;
            break;
    }
    //dbg("After : 0x%x", m.reg[7]);
}

static u8 machine_get_flag(char c){
    switch(c){
        case 'S':
            return (m.reg[7] >> 7) & 1;
        case 'Z':
            return (m.reg[7] >> 6) & 1 ;
        case 'A':
            return (m.reg[7] >> 4) & 1;
        case 'P':
            return (m.reg[7] >> 2) & 1;
        case 'C':
            return m.reg[7] & 1;
        default:
            err("Unknown flag requested : %c!", c);
            return 0;
    }
}

static void pbin8(u8 val){
    u8 i = 0;
    u16 a = (1 << 7);
    while(i < 8){
        printf("%d", (val & a) >> (7 - i));
        a >>= 1;
        i++;
        if(i % 4 == 0)
            printf(" ");
    }
}

void machine_print(){
    pgrnh("\n[Accumulator] ", "0x%x (", m.reg[6]);
    pbin8(m.reg[6]);
    printf("\b)");
    pblueh("\n[Flags] ", " S : %u\tZ : %u\tAC : %u\tP : %u\tCY : %u ", 
            machine_get_flag('S'), machine_get_flag('Z'), 
            machine_get_flag('A'), machine_get_flag('P'),
            machine_get_flag('C'));
    pylwh("\n[Registers]", " ");
    const char names[] = {'B', 'C', 'D', 'E', 'H', 'L'};
    for(u8 i = 0;i < 6;i++){
        printf("%c : 0x%x ", names[i], m.reg[i]);
    }
    pylwh("\n[Program Counter] ", "0x%x", m.pc);
    pylwh("\n[Stack Pointer] ", "0x%x", m.sp);
}

void machine_init(){
    m.reg[6] = 0;
    m.reg[7] = 0;
    m.pc = 0;
    for(u8 i = 0;i < 6;i++)
        m.reg[i] = 0;
    m.sp = 0xffff;
}

static void machine_check_and_update_flags(){
    machine_set_flag('S', m.reg[6] >> 7);
    machine_set_flag('Z', m.reg[6] == 0);
    u8 no1 = 0;
    u16 val = m.reg[6];
    while(val != 0){
        no1 += val & 1;
        val >>= 1;
    }
    machine_set_flag('P',~( no1 & 1));
}

static u16 gen_mem(u8 i){
    u16 res = mem[i];
    res <<= 8;
    res |= mem[i+1];
    return res;
}

void mem_write(u16 add, u8 val){
    mem[add] = val;
}

typedef struct{
    u8 res;
    //u8 ac;
    u8 c;
} Result;

static Result addc8(u8 a, u8 b, u8 c){
    Result r;
    r.c = c;
    r.res = 0;
    //r.ac = 0;
    for(u8 i = 0;i < 8;i++){
        u8 b1 = (a >> i) & 1;
        u8 b2 = (b >> i) & 1;
        //dbg("a : %d\tb : %d\tc : %d", b1, b2, r.c);
        r.res |= ((b1 ^ b2 ^ r.c) << i);
        //dbg("res : 0x%x", r.res);
        r.c = (b1 & b2) | (r.c & (b1 | b2));
        //dbg("carry : 0x%x", r.c);
    }
    return r; 
}

static Result add8(u8 a, u8 b){
    return addc8(a, b, 0);
}

static Result subb8(u8 a, u8 b, u8 c){
    Result r;
    r.c = c;
    r.res = 0;
    //r.ac = 0;
    for(u8 i = 0;i < 8;i++){
        u8 b1 = (a >> i) & 1;
        u8 b2 = (b >> i) & 1;
        //dbg("a : %d\tb : %d\tc : %d", b1, b2, r.c);
        r.res |= ((b1 ^ b2 ^ r.c) << i);
        //dbg("res : 0x%x", r.res);
        r.c = (~b1 & b2) | (r.c & (~b1 | b2));
        //dbg("carry : 0x%x", r.c);
    }
    return r; 
}

static Result sub8(u8 a, u8 b){
    return subb8(a, b, 0);
}

static inline u16 u8to16(u8 a, u8 b){
    return ((u16)a << 8) | b;
}

static inline u16 gen_mem_from_hl(){
    return u8to16(m.reg[4], m.reg[5]);
}

static const char *opcode_string[] = { 
    #define INSTRUCTION(name, range1, range2) #name,
    #include "instruction.h"
    #undef INSTRUCTION
};

const char* machine_get_opcode_string(u8 opcode){
    return opcode_string[opcode];
}

void machine_run(){
    while(1){
        //pgrnh("\n[Program Counter] ", "0x%x", m.pc);
        //pgrnh("\n[mem] ", "0x%x", mem[m.pc]);
        machine_print();
        printf("\n");
        Instruction ins = decode_ins(&mem[m.pc]);
        pblueh("\n[instruction] ", "0x%x (%s)", mem[m.pc], machine_get_opcode_string(ins.op));
        //getchar();
        switch(ins.op){
            case HLT:
                return;
            case NOP:
                break;
            case CMP:
                {
                    m.tmp = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem(m.pc + 1)];
                    if(m.regi[6] < m.tmpi)
                        machine_set_flag('C', 1);
                    else if(m.regi[6] == m.tmpi)
                        machine_set_flag('Z', 1);
                    else{
                        machine_set_flag('C', 0);
                        machine_set_flag('Z', 0);
                    }
                    m.pc += 2;
                }
                break;
            case CPI:
                {
                    m.tmp = mem[m.pc + 1];
                    //dbg("tmp : %" Pu8 "\tacc : %" Pu8, m.tmp, m.reg[6]);
                    if(m.regi[6] < m.tmpi)
                        machine_set_flag('C', 1);
                    else if(m.reg[6] == m.tmp)
                        machine_set_flag('Z', 1);
                    else{
                        machine_set_flag('C', 0);
                        machine_set_flag('Z', 0);
                    }
                    m.pc += 1;
                }
                break;
            case ANA:
                {
                    m.tmp = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem_from_hl()];
                    m.reg[6] &= m.tmp;
                    machine_check_and_update_flags();
                    machine_set_flag('C', 0);
                    machine_set_flag('A', 1);
                }
                break;
            case ANI:
                {
                    m.reg[6] &= mem[m.pc + 1];
                    machine_check_and_update_flags();
                    machine_set_flag('C', 0);
                    machine_set_flag('A', 1);
                    m.pc += 1;
                }
                break;
            case XRA:
                {
                    m.tmp = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem_from_hl()];
                    m.reg[6] ^= m.tmp;
                    machine_check_and_update_flags();
                    machine_set_flag('C', 0);
                    machine_set_flag('A', 0);
                }
                break;
            case XRI:
                {
                    m.reg[6] ^= mem[m.pc + 1];
                    machine_check_and_update_flags();
                    machine_set_flag('C', 0);
                    machine_set_flag('A', 0);
                    m.pc += 1;
                }
                break;
            case ORA:
                {
                    m.tmp = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem_from_hl()];
                    m.reg[6] |= m.tmp;
                    machine_check_and_update_flags();
                    machine_set_flag('C', 0);
                    machine_set_flag('A', 0);    
                }
                break;
            case ORI:
                {
                    m.reg[6] |= mem[m.pc + 1];
                    machine_check_and_update_flags();
                    machine_set_flag('C', 0);
                    machine_set_flag('A', 0);
                    m.pc += 1;
                }
                break;
            case RLC:
                {
                    m.tmp = m.reg[6] & (1 << 7);
                    m.reg[6] <<= 1;
                    m.reg[6] |= m.tmp;
                    machine_set_flag('C', m.tmp);
                }
                break;
            case RRC:
                {
                    m.tmp = m.reg[6] & 1;
                    m.reg[6] >>= 1;
                    m.reg[6] |= (m.tmp << 7);
                    machine_set_flag('C', m.tmp);
                }
                break;
            case RAL:
                {
                    m.tmp = machine_get_flag('C');
                    machine_set_flag('C', m.reg[6] & (1 << 7));
                    m.reg[6] <<= 1;
                    m.reg[6] |= m.tmp;
                }
                break;
            case RAR:
                {
                    m.tmp = machine_get_flag('C');
                    machine_set_flag('C', m.reg[6] & 1);
                    m.reg[6] >>= 1;
                    m.reg[6] |= (m.tmp << 7);
                }
                break;
            case CMA:
                {
                    m.reg[6] = ~m.reg[6];
                }
                break;
            case CMC:
                {
                    machine_set_flag('C', ~machine_get_flag('C'));
                }
                break;
            case STC:
                {
                    machine_set_flag('C', 1);
                }
                break;
                
            case JMP:
                {
op_jmp:
                    m.pc = u8to16(mem[m.pc + 1], mem[m.pc + 2]);
                    continue;
                }
            #define JUMP(name, cond) \
            case name: \
                { \
                    if(cond) \
                        goto op_jmp; \
                    m.pc += 2; \
                } \
                break;
            JUMP(JC, machine_get_flag('C'))
            JUMP(JNC, !machine_get_flag('C'))
            JUMP(JP, !machine_get_flag('S'))
            JUMP(JM, machine_get_flag('S'))
            JUMP(JZ, machine_get_flag('Z'))
            JUMP(JNZ, !machine_get_flag('Z'))
            JUMP(JPE, machine_get_flag('P'))
            JUMP(JPO, !machine_get_flag('P'))
            #undef JUMP
            case CALL:
                {
op_call:;
                    u16 nxt = m.pc + 2;
                    mem[m.sp - 1] = nxt & 0x00ff;
                    mem[m.sp - 2] = nxt & 0xff00;
                    m.sp -= 2;
                    m.pc = gen_mem(m.pc + 1);
                }
                break;
            #define CALL(name, cond) \
            case name: \
                { \
                    if(cond) \
                        goto op_call; \
                    m.pc += 2; \
                } \
                break;
            CALL(CC, machine_get_flag('C'))
            CALL(CNC, !machine_get_flag('C'))
            CALL(CP, !machine_get_flag('S'))
            CALL(CM, machine_get_flag('S'))
            CALL(CZ, machine_get_flag('Z'))
            CALL(CNZ, !machine_get_flag('Z'))
            CALL(CPE, machine_get_flag('P'))
            CALL(CPO, !machine_get_flag('P'))
            #undef CALL
            case RET:
                {
op_ret:
                    m.pc = u8to16(mem[m.sp], mem[m.sp + 1]);
                    m.sp += 2;
                    continue;
                }
            #define RET(name, cond) \
            case name: \
                { \
                    if(cond) \
                        goto op_ret; \
                    m.pc += 2; \
                } \
                break;
            RET(RC, machine_get_flag('C'))
            RET(RNC, !machine_get_flag('C'))
            RET(RP, !machine_get_flag('S'))
            RET(RM, machine_get_flag('S'))
            RET(RZ, machine_get_flag('Z'))
            RET(RNZ, !machine_get_flag('Z'))
            RET(RPE, machine_get_flag('P'))
            RET(RPO, !machine_get_flag('P'))
            #undef RET
            case PCHL:
                {
                    m.pc = u8to16(m.reg[4], m.reg[5]);
                    continue;
                }
            #define RST(num, add) \
            case RST_##num: \
                { \
                    m.pc = 0x00##add; \
                    continue; \
                }
            RST(0, 0)
            RST(1, 08)
            RST(2, 10)
            RST(3, 18)
            RST(4, 20)
            RST(5, 28)
            RST(6, 30)
            RST(7, 38)

            case ADD:
                {
                    m.tmpi = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem_from_hl()];
                    Result r = add8(m.regi[6], m.tmpi);
                    //dbg("Result : %" Pi8 "\tCarry : %" Pu8, (i8)r.res, r.c);
                    machine_set_flag('C', r.c);
                    machine_set_flag('A', (m.regi[6] & 0xf) + (m.tmpi &0xf) > 0xf);
                    m.reg[6] = r.res;
                    machine_check_and_update_flags();
                }
                break;
            case ADC:
                {
                    m.tmpi = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem_from_hl()]; 
                    Result r = addc8(m.regi[6], m.tmpi, machine_get_flag('C'));
                    //dbg("Result : %" Pi8 "\tCarry : %" Pu8, (i8)r.res, r.c);
                    machine_set_flag('C', r.c);
                    machine_set_flag('A', (m.regi[6] & 0xf) + (m.tmpi & 0xf) > 0xf);
                    m.reg[6] = r.res;
                    machine_check_and_update_flags();
                }
                break;
            case ADI:
                {
                    m.tmpi = mem[m.pc + 1];
                    Result r = add8(m.regi[6], m.tmpi);
                    //dbg("Result : %" Pi8 "\tCarry : %" Pu8, (i8)r.res, r.c);
                    machine_set_flag('C', r.c);
                    machine_set_flag('A', (m.regi[6] & 0xf) + (m.tmpi & 0xf) > 0xf);
                    m.reg[6] = r.res;
                    machine_check_and_update_flags();
                    m.pc++;
                }
                break;
            case ACI:
                { 
                    m.tmpi = mem[m.pc + 1];
                    Result r = addc8(m.regi[6], m.tmpi, machine_get_flag('C'));
                    //dbg("Result : %" Pi8 "\tCarry : %" Pu8, (i8)r.res, r.c);
                    machine_set_flag('C', r.c);
                    machine_set_flag('A', (m.regi[6] & 0xf) + (m.tmpi & 0xf) > 0xf);
                    m.reg[6] = r.res;
                    machine_check_and_update_flags();
                    m.pc++; 
                }
                break;
            #define LXI(r, idx1, idx2) \
            case LXI_##r: \
                { \
                    m.reg[idx1] = mem[m.pc + 1]; \
                    m.reg[idx2] = mem[m.pc + 2]; \
                    m.pc += 2; \
                } \
                break;
            LXI(B, 0, 1)
            LXI(D, 2, 3)
            LXI(H, 4, 5)
            #undef LXI
            #define DAD(r, idx1, idx2) \
            case DAD_##r: \
                { \
                    Result r = add8(m.reg[idx2], m.reg[5]); \
                    Result r2 = addc8(m.reg[idx1], m.reg[4], r.c); \
                    m.reg[4] = r2.res; \
                    m.reg[5] = r.res; \
                    machine_set_flag('C', r2.c); \
                } \
                break;
            DAD(B, 0, 1)
            DAD(D, 2, 3)
            DAD(H, 4, 5)
            #undef DAD
            case SUB:
                {
                    m.tmpi = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem_from_hl()];
                    Result r = sub8(m.regi[6], m.tmpi);
                    //dbg("Result : %" Pi8 "\tCarry : %" Pu8, (i8)r.res, r.c);
                    machine_set_flag('C', r.c);
                    machine_set_flag('A', (m.regi[6] & 0xf) + (m.tmpi &0xf) > 0xf);
                    m.reg[6] = r.res;
                    machine_check_and_update_flags();
                }
                break;
            case SBB:
                {
                    m.tmpi = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem_from_hl()]; 
                    Result r = subb8(m.regi[6], m.tmpi, machine_get_flag('C'));
                    //dbg("Result : %" Pi8 "\tCarry : %" Pu8, (i8)r.res, r.c);
                    machine_set_flag('C', r.c);
                    machine_set_flag('A', (m.regi[6] & 0xf) < (m.tmpi & 0xf));
                    m.regi[6] = r.res;
                    machine_check_and_update_flags();
                }
                break;
            case SUI:
                {
                    m.tmpi = mem[m.pc + 1];
                    Result r = sub8(m.regi[6], m.tmpi);
                    //dbg("Result : %" Pi8 "\tCarry : %" Pu8, (i8)r.res, r.c);
                    machine_set_flag('C', r.c);
                    machine_set_flag('A', (m.regi[6] & 0xf) < (m.tmpi & 0xf));
                    m.reg[6] = r.res;
                    machine_check_and_update_flags();
                    m.pc++;
                }
                break;
            case SBI:
                { 
                    m.tmpi = mem[m.pc + 1];
                    Result r = subb8(m.regi[6], m.tmpi, machine_get_flag('C'));
                    //dbg("Result : %" Pi8 "\tCarry : %" Pu8, (i8)r.res, r.c);
                    machine_set_flag('C', r.c);
                    machine_set_flag('A', (m.regi[6] & 0xf) < (m.tmpi & 0xf));
                    m.reg[6] = r.res;
                    machine_check_and_update_flags();
                    m.pc++; 
                }
                break;
            #define INR(r, n) \
            case INR_##r: \
                { \
                    m.reg[n]++; \
                } \
                break;
            INR(B, 0)
            INR(C, 1)
            INR(D, 2)
            INR(E, 3)
            INR(H, 4)
            INR(L, 5)
            INR(A, 6)
            #undef INR
            case INR_M:
                {
                    mem[gen_mem_from_hl()]++;
                }
                break;
            #define INX(r, n) \
            case INX_##r: \
                { \
                    Result r = add8(m.reg[n + 1], 1); \
                    Result r2 = add8(m.reg[n], r.c); \
                    m.reg[n + 1] = r.res; \
                    m.reg[n] = r2.res; \
                } \
                break;
            INX(B, 0)
            INX(D, 2)
            INX(H, 4)
            #undef INX
            #define DCR(r, n) \
            case DCR_##r: \
                { \
                    m.reg[n]--; \
                } \
                break;
            DCR(B, 0)
            DCR(C, 1)
            DCR(D, 2)
            DCR(E, 3)
            DCR(H, 4)
            DCR(L, 5)
            DCR(A, 6)
            #undef DCR
            case DCR_M:
                {
                    mem[gen_mem_from_hl()]--;
                }
            #define DCX(r, n) \
            case DCX_##r: \
                { \
                    Result r = sub8(m.reg[n + 1], 1); \
                    Result r2 = sub8(m.reg[n], r.c); \
                    m.reg[n + 1] = r.res; \
                    m.reg[n] = r2.res; \
                } \
                break;
            DCX(B, 0)
            DCX(D, 2)
            DCX(H, 4)
            #undef DCX
            case DAA:
                {
                    m.tmp = m.reg[6];
                    u8 bak = machine_get_flag('C');
                    machine_set_flag('C', 0);
                    if(machine_get_flag('A') || (m.reg[6] & 0x0f) > 9){
                        Result r = add8(m.reg[6], 6);
                        machine_set_flag('C', bak || r.c);
                        m.reg[6] = r.res;
                        machine_set_flag('A', 1);
                    }
                    else
                        machine_set_flag('A', 0);
                    if(bak || m.tmp > 0x99){
                        Result r = add8(m.reg[6], 0x60);
                        m.reg[6] = r.res;
                        machine_set_flag('C', 1);
                    }
                    else
                        machine_set_flag('C', 0);
                }
                break;

            case MOV:
                {
                    u8 val = ins.operand0 ? m.reg[ins.reg[0]] : mem[gen_mem_from_hl()];
                    ins.operand1 ? (m.reg[ins.reg[1]] = val) 
                                : (mem[gen_mem_from_hl()] = val);
                }
                break;
            #define MVI(r, n) \
            case MVI_##r: \
                { \
                    m.reg[n] = mem[m.pc + 1]; \
                    m.pc += 1; \
                } \
                break;
            MVI(B, 0)
            MVI(C, 1)
            MVI(D, 2)
            MVI(E, 3)
            MVI(H, 4)
            MVI(L, 5)
            MVI(A, 6)
            #undef MVI
            case MVI_M:
                {
                    mem[gen_mem_from_hl()] = mem[m.pc + 1];
                    m.pc += 1;
                }
                break;
            case LDA:
                {
                    m.reg[6] = mem[u8to16(mem[m.pc + 1], mem[m.pc + 2])];
                    m.pc += 2;
                }
                break;
            #define LDAX(r, n) \
            case LDAX_##r: \
                { \
                    m.reg[6] = mem[u8to16(m.reg[n], m.reg[n+1])]; \
                } \
                break;
            LDAX(B, 0)
            LDAX(D, 1)
            #undef LDAX
            case LHLD:
                {
                    m.reg[5] = mem[m.pc + 1];
                    m.reg[4] = mem[m.pc + 2];
                    m.pc += 2;
                }
                break;
            case STA:
                {
                    mem[u8to16(mem[m.pc + 2], mem[m.pc + 1])] = m.reg[6];
                    m.pc += 2;
                }
                break;
            #define STAX(r, n) \
            case STAX_##r: \
                { \
                    mem[u8to16(m.reg[n], m.reg[n+1])] = m.reg[6]; \
                } \
                break;
            STAX(B, 0)
            STAX(D, 2)
            #undef STAX
            case SHLD:
                {
                    u16 add = u8to16(mem[m.pc + 2], mem[m.pc + 1]);
                    mem[add] = m.reg[5];
                    mem[add+1] = m.reg[4];
                }
                break;
            case XCHG:
                {
                    m.reg[3] ^= m.reg[5];
                    m.reg[5] ^= m.reg[3];
                    m.reg[3] ^= m.reg[5];

                    m.reg[2] ^= m.reg[4];
                    m.reg[4] ^= m.reg[2];
                    m.reg[2] ^= m.reg[4];
                }
                break;
            case SPHL:
                {
                    m.sp = u8to16(m.reg[4], m.reg[5]);
                }
                break;
            case XTHL:
                {
                    m.reg[5] = mem[m.sp];
                    m.reg[4] = mem[m.sp + 1];
                }
                break;
            #define PUSH(r, n) \
            case PUSH_##r: \
                { \
                    mem[m.sp - 1] = m.reg[n]; \
                    mem[m.sp - 2] = m.reg[n + 1]; \
                    m.sp -= 2; \
                } \
                break;
            PUSH(B, 0)
            PUSH(D, 2)
            PUSH(H, 4)
            PUSH(A, 6)
            #undef PUSH
            #define POP(r, n) \
            case POP_##r: \
                { \
                    m.reg[n + 1] = mem[m.sp]; \
                    m.reg[n] = mem[m.sp + 1]; \
                    m.sp += 2; \
                } \
                break;
            POP(B, 0)
            POP(D, 2)
            POP(H, 4)
            POP(A, 6)
            #undef POP
            case OUT:
                {
                    putc(mem[m.pc + 1], stdout);
                    m.pc++;
                }
                break;
            case IN:
                {
                    m.reg[6] = (u8)getchar();
                }
                break;
                
            default:
                err("Operation not implemented : 0x%x", ins.op);
        }
        m.pc++;
    }
}
