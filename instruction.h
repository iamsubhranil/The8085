// INSTRUCTION(opcode_name, opcode_start, opcode_end)


#define INSTRUCTION1(x, y) INSTRUCTION(x, y, y)
        /* Control flow */

    // No operation
    // No operation is performed, i.e., the instruction is fetched and decoded.
    INSTRUCTION1(NOP, 0x00)

    // Halt 
    // The CPU finishes executing the current instruction and stops further 
    // execution.     
    // An interrupt or reset is necessary to exit from the halt state.
    INSTRUCTION1(HLT, 0x76)

    // Disable interrupts
    // The interrupt enable flip-flop is reset and all the interrupts are 
    // disabled except TRAP.
    INSTRUCTION1(DI, 0xf3)

    // Enable interrupts
    // The interrupt enable flip-flop is set and all the interrupts are enabled.
    INSTRUCTION1(EI, 0xfb)

    // Read interrupt mask 
    // This instruction is used to read the status of interrupts 7.5, 6.5, 5.5 
    // and read serial data input bit.
    INSTRUCTION1(RIM, 0x20)

    // Set interrupt mask
    // This instruction is used to implement the interrupts 7.5, 6.5, 5.5, and 
    // serial data output.
    INSTRUCTION1(SIM, 0x30)

        /* Logical Instructions */

    // Compare the register or memory with the accumulator
    // The contents of the operand (register or memory) are M compared with the
    // contents of the accumulator.
    INSTRUCTION(CMP, 0xb8, 0xbf)

    // Compare immediate with the accumulator
    // The second byte data is compared with the contents of the accumulator.
    INSTRUCTION1(CPI, 0xfe)

    // Logical AND register or memory with the accumulator
    // The contents of the accumulator are logically AND with M the contents of
    // the register or memory, and the result is placed in the accumulator.
    INSTRUCTION(ANA, 0xa0, 0xa7)

    // Logical AND immediate with the accumulator
    // The contents of the accumulator are logically AND with the 8-bit data 
    // and the result is placed in the accumulator.
    INSTRUCTION1(ANI, 0xe6)

    // Exclusive OR register or memory with the accumulator 
    // The contents of the accumulator are Exclusive OR with M the contents of 
    // the register or memory, and the result is placed in the accumulator.
    INSTRUCTION(XRA, 0xa8, 0xaf)

    // Exclusive OR immediate with the accumulator
    // The contents of the accumulator are Exclusive OR with the 8-bit data and
    // the result is placed in the accumulator.
    INSTRUCTION1(XRI, 0xee)

    // Logical OR register or memory with the accumulator
    // The contents of the accumulator are logically OR with M the contents of 
    // the register or memory, and result is placed in the accumulator.
    INSTRUCTION(ORA, 0xb0, 0xb7)

    // Logical OR immediate with the accumulator
    // The contents of the accumulator are logically OR with the 8-bit data and
    // the result is placed in the accumulator.
    INSTRUCTION1(ORI, 0xf6)

    // Rotate the accumulator left
    // Each binary bit of the accumulator is rotated left by one position.
    // Bit D7 is placed in the position of D0 as well as in the Carry flag.
    // CY is modified according to bit D7.
    INSTRUCTION1(RLC, 0x07)

    // Rotate the accumulator right
    // Each binary bit of the accumulator is rotated right by one position.
    // Bit D0 is placed in the position of D7 as well as in the Carry flag.
    // CY is modified according to bit D0.
    INSTRUCTION1(RRC, 0x0f)

    // Rotate the accumulator left through carry
    // Each binary bit of the accumulator is rotated left by one position 
    // through the Carry flag. 
    // Bit D7 is placed in the Carry flag, and the Carry flag is placed in the 
    // least significant position D0.
    // CY is modified according to bit D7.
    INSTRUCTION1(RAL, 0x17)

    // Rotate the accumulator right through carry
    // Each binary bit of the accumulator is rotated right by one position 
    // through the Carry flag.
    // Bit D0 is placed in the Carry flag, and the Carry flag is placed in the 
    // most significant position D7.
    // CY is modified according to bit D0.
    INSTRUCTION1(RAR, 0x1f)

    // Complement accumulator
    // The contents of the accumulator are complemented. No flags are affected.
    INSTRUCTION1(CMA, 0x2f)

    // Complement carry
    // The Carry flag is complemented. No other flags are affected.
    INSTRUCTION1(CMC, 0x3f)

    // Set Carry
    // Set Carry
    INSTRUCTION1(STC, 0x37)

        /* Branching Instructions */

    // Jump unconditionally
    // The program sequence is transferred to the memory address given in the 
    // operand.
    INSTRUCTION1(JMP, 0xc3)

    // Jump on Carry
    // Jump when CY=1
    // The program sequence is transferred to the memory address given
    // in the operand based on the specified flag of the PSW.
    INSTRUCTION1(JC, 0xda)

    // Jump on no Carry
    // Jump when CY=0
    INSTRUCTION1(JNC, 0xd2)

    // Jump on positive
    // Jump when S=0
    INSTRUCTION1(JP, 0xf2)

    // Jump on minus
    // Jump when S=1
    INSTRUCTION1(JM, 0xfa)

    // Jump on zero
    // Jump when Z=1
    INSTRUCTION1(JZ, 0xca)

    // Jump on no zero
    // Jump when Z=0
    INSTRUCTION1(JNZ, 0xc2)

    // Jump on parity even
    // Jump when P=1
    INSTRUCTION1(JPE, 0xea)

    // Jump on parity odd
    // Jump when P=0
    INSTRUCTION1(JPO, 0xe2)

    // Subroutine calls
    
    // Unconditional subroutine call
    // The program sequence is transferred to the memory address given in the 
    // operand. Before transferring, the address of the next instruction after 
    // CALL is pushed onto the stack.
    INSTRUCTION1(CALL, 0xcd)

    // Call on Carry
    // Call when CY = 1
    INSTRUCTION1(CC, 0xdc)

    // Call on no Carry
    // Call when CY = 0
    INSTRUCTION1(CNC, 0xd4)

    // Call on positive
    // Call when S = 0
    INSTRUCTION1(CP, 0xf4)

    // Call on minus
    // Call when S = 1
    INSTRUCTION1(CM, 0xfc)

    // Call on zero
    // Call when Z = 1
    INSTRUCTION1(CZ, 0xcc)

    // Call on no zero
    // Call when Z = 0
    INSTRUCTION1(CNZ, 0xc4)

    // Call on parity even
    // Call when P = 1
    INSTRUCTION1(CPE, 0xec)

    // Call on parity odd
    // Call when P = 0
    INSTRUCTION1(CPO, 0xe4)

    // Return from subroutine unconditionally
    // The program sequence is transferred from the subroutine to the calling 
    // program.
    INSTRUCTION1(RET, 0xc9)

    // Return on Carry
    // Return when CY=1
    // The program sequence is transferred from the subroutine to the calling 
    // program based on the specified flag of the PSW and the program execution
    // begins at the new address.
    INSTRUCTION1(RC, 0xd8)

    // Return on no Carry
    // Return when CY=0
    INSTRUCTION1(RNC, 0xd0)

    // Return on positive
    // Return when S=0
    INSTRUCTION1(RP, 0xf0)

    // Return on minus
    // Return when S=1
    INSTRUCTION1(RM, 0xf8)

    // Return on zero
    // Return when Z=1
    INSTRUCTION1(RZ, 0xc8)

    // Return on no zero
    // Return when Z=0
    INSTRUCTION1(RNZ, 0xc0)

    // Return on parity eve
    // Return when P=1
    INSTRUCTION1(RPE, 0xe8)

    // Return on parity odd
    // Return when P=0
    INSTRUCTION1(RPO, 0xe0)

    // Load the program counter with HL contents
    // The contents of registers H & L are copied into the program counter.
    // The contents of H are placed as the high-order byte and the contents of 
    // L as the loworder byte.
    INSTRUCTION1(PCHL, 0xe9)

    // Restart
    // The RST instruction is used as software instructions in a program 
    // to transfer the program execution to one of the following eight 
    // locations : 
    // Instruction  Restart Address
    // RST 0 	        0000H
    // RST 1 	        0008H
    // RST 2 	        0010H
    // RST 3 	        0018H
    // RST 4 	        0020H
    // RST 5 	        0028H
    // RST 6 	        0030H
    // RST 7 	        0038H
    // The 8085 has additionally 4 interrupts, which can generate RST 
    // instructions internally and doesn’t require any external hardware.
    // Following are those instructions and their Restart addresses −
    // Interrupt 	Restart Address
    // TRAP 	        0024H
    // RST 5.5 	        002CH
    // RST 6.5 	        0034H
    // RST 7.5 	        003CH
    INSTRUCTION1(RST_0, 0xc7)
    INSTRUCTION1(RST_1, 0xcf)
    INSTRUCTION1(RST_2, 0xd7)
    INSTRUCTION1(RST_3, 0xdf)
    INSTRUCTION1(RST_4, 0xe7)
    INSTRUCTION1(RST_5, 0xef)
    INSTRUCTION1(RST_6, 0xf7)
    INSTRUCTION1(RST_7, 0xff)

        /* Arithmetic Operations */

    // Add register or memory, to the accumulator
	// The contents of the register or memory are added to the contents of
	// the accumulator and the result is stored in the accumulator.
    // Example − ADD K.
    INSTRUCTION(ADD, 0x80, 0x87)

	// Add register to the accumulator with carry
    // The contents of the register or memory & M the Carry flag are added to 
    // the contents of the accumulator and the result is stored in the 
    // accumulator.
    // Example − ADC K
    INSTRUCTION(ADC, 0x88, 0x8f)

    // Add the immediate to the accumulator
    // The 8-bit data is added to the contents of the accumulator and the 
    // result is stored in the accumulator.
    // Example − ADI 55K
    INSTRUCTION1(ADI, 0xc6)

    // Add the immediate to the accumulator with carry
    // The 8-bit data and the Carry flag are added to the contents of 
    // the accumulator and the result is stored in the accumulator.
    // Example − ACI 55K
    INSTRUCTION1(ACI, 0xce)

    // Load the register pair immediate
    // The instruction stores 16-bit data into the register pair designated in 
    // the operand.
    // Example − LXI K, 3025M
    INSTRUCTION1(LXI_B, 0x01)
    INSTRUCTION1(LXI_D, 0x11)
    INSTRUCTION1(LXI_H, 0x21)
    INSTRUCTION1(LXI_SP, 0x31)

    // Add the register pair to H and L registers
    // The 16-bit data of the specified register pair are added to the contents
    // of the HL register.
    // Example − DAD K
    INSTRUCTION1(DAD_B, 0x09)
    INSTRUCTION1(DAD_D, 0x19)
    INSTRUCTION1(DAD_H, 0x29)
    INSTRUCTION1(DAD_SP, 0x39)

    // Subtract the register or the memory from the accumulator
    // The contents of the register or the memory are subtracted from the 
    // contents of the accumulator, and the result is stored in the accumulator.
    // Example − SUB K
    INSTRUCTION(SUB, 0x90, 0x97)

    // Subtract the source and borrow from the accumulator
    // The contents of the register or the memory & M the Borrow flag are 
    // subtracted from the contents of the accumulator and the result is placed
    // in the accumulator.
    // Example − SBB K
    INSTRUCTION(SBB, 0x98, 0x9f)

    // Subtract the immediate from the accumulator
    // The 8-bit data is subtracted from the contents of the accumulator & the
    // result is stored in the accumulator.
    // Example − SUI 55K
    INSTRUCTION1(SUI, 0xd6)

    // Subtract the immediate from the accumulator with borrow
    // The contents of register H are exchanged with the contents of register D,
    // and the contents of register L are exchanged with the contents of 
    // register E.
    // Example − XCHG
    INSTRUCTION1(SBI, 0xde)

    // Increment the register or the memory by 1
    // The contents of the designated register or the memory are incremented by 
    // 1 and their result is stored at the same place.
    // Example − INR K
    INSTRUCTION1(INR_A, 0x3c)
    INSTRUCTION1(INR_B, 0x04)
    INSTRUCTION1(INR_C, 0x0c)
    INSTRUCTION1(INR_D, 0x14)
    INSTRUCTION1(INR_E, 0x1c)
    INSTRUCTION1(INR_H, 0x24)
    INSTRUCTION1(INR_L, 0x2c)
    INSTRUCTION1(INR_M, 0x34)

    // Increment register pair by 1
    // The contents of the designated register pair are incremented by 1 and 
    // their result is stored at the same place.
    // Example − INX K
    INSTRUCTION1(INX_B, 0x03)
    INSTRUCTION1(INX_D, 0x13)
    INSTRUCTION1(INX_H, 0x23)
    INSTRUCTION1(INX_SP, 0x33)

    // Decrement the register or the memory by 1
    // The contents of the designated register or memory are decremented by 1 
    // and their result is stored at the same place.
    // Example − DCR K
    INSTRUCTION1(DCR_A, 0x3d)
    INSTRUCTION1(DCR_B, 0x05)
    INSTRUCTION1(DCR_C, 0x0d)
    INSTRUCTION1(DCR_D, 0x1d)
    INSTRUCTION1(DCR_E, 0x15)
    INSTRUCTION1(DCR_H, 0x25)
    INSTRUCTION1(DCR_L, 0x2d)
    INSTRUCTION1(DCR_M, 0x35)

    // Decrement the register pair by 1
    // The contents of the designated register pair are decremented by 1 and 
    // their result is stored at the same place.
    // Example − DCX K
    INSTRUCTION1(DCX_B, 0x0b)
    INSTRUCTION1(DCX_D, 0x1b)
    INSTRUCTION1(DCX_H, 0x2b)
    INSTRUCTION1(DCX_SP, 0x3b)

    // Decimal adjust accumulator
    // The contents of the accumulator are changed from a binary value to two 
    // 4-bit BCD digits. If the value of the low-order 4-bits in the accumulator
    // is greater than 9 or if AC flag is set, the instruction adds 6 to the 
    // low-order four bits. If the value of the high-order 4-bits in the 
    // accumulator is greater than 9 or if the Carry flag is set, the 
    // instruction adds 6 to the high-order four bits.
    // Example − DAA
    INSTRUCTION1(DAA, 0x27)

        /* Data Transfer Instructions */

    // Copy from the source (Sc) to the destination(Dt)
    // This instruction copies the contents of the source register into the 
    // destination register without any alteration.
    // Example − MOV K, L
    INSTRUCTION(MOV, 0x40, 0x7f)

    // Move immediate 8-bit
    // The 8-bit data is stored in the destination register or memory.
    // Example − MVI K, 55L
    INSTRUCTION1(MVI_A, 0x3e)
    INSTRUCTION1(MVI_B, 0x06)
    INSTRUCTION1(MVI_C, 0x0e)
    INSTRUCTION1(MVI_D, 0x16)
    INSTRUCTION1(MVI_E, 0x1e)
    INSTRUCTION1(MVI_H, 0x26)
    INSTRUCTION1(MVI_L, 0x2e)
    INSTRUCTION1(MVI_M, 0x36)

    // Load the accumulator
    // The contents of a memory location, specified by a 16-bit address in the 
    // operand, are copied to the accumulator.
    // Example − LDA 2034K
    INSTRUCTION1(LDA, 0x3a)

    // Load the accumulator indirect
    // The contents of the designated register pair point to a memory location.
    // This instruction copies the contents of that memory location into the 
    // accumulator.
    // Example − LDAX K
    INSTRUCTION1(LDAX_B, 0x0a)
    INSTRUCTION1(LDAX_D, 0x1a)

    // Load H and L registers direct
    // The instruction copies the contents of the memory location pointed out 
    // by the address into register L and copies the contents of the next 
    // memory location into register H.
    // Example − LHLD 3225K
    INSTRUCTION1(LHLD, 0x2a)

    // The contents of the accumulator are copied into the memory location 
    // specified by the operand. This is a 3-byte instruction, the second byte 
    // specifies the low-order address and the third byte specifies the 
    // high-order address.
    // Example − STA 325K
    INSTRUCTION1(STA, 0x32)

    // Store the accumulator indirect
    // The contents of the accumulator are copied into the memory location 
    // specified by the contents of the operand.
    // Example − STAX K
    INSTRUCTION1(STAX_B, 0x02)
    INSTRUCTION1(STAX_D, 0x12)

    // Store H and L registers direct
    // The contents of register L are stored in the memory location specified 
    // by the 16-bit address in the operand and the contents of H register are 
    // stored into the next memory location by incrementing the operand.
    // This is a 3-byte instruction, the second byte specifies the low-order 
    // address and the third byte specifies the high-order address.
    // Example − SHLD 3225K
    INSTRUCTION1(SHLD, 0x22)

    // Exchange H and L with D and E
    // The contents of register H are exchanged with the contents of register D
    // and the contents of register L are exchanged with the contents of 
    // register E.
    // Example − XCHG
    INSTRUCTION1(XCHG, 0xeb)

    // Copy H and L registers to the stack pointer
    // The instruction loads the contents of the H and L registers into the 
    // stack pointer register. The contents of the H register provide the 
    // high-order address and the contents of the L register provide the 
    // low-order address.
    // Example − SPHL
    INSTRUCTION1(SPHL, 0xf9)

    // Exchange H and L with top of stack
    // The contents of the L register are exchanged with the stack location 
    // pointed out by the contents of the stack pointer register.
    // The contents of the H register are exchanged with the next stack 
    // location (SP+1).
    // Example − XTHL
    INSTRUCTION1(XTHL, 0xe3)

    // Push the register pair onto the stack
    // The contents of the register pair designated in the operand are copied 
    // onto the stack in the following sequence : 
    // 1. The stack pointer register is decremented and the contents of the 
    // high order register (B, D, H, A) are copied into that location.
    // 2. The stack pointer register is decremented again and the contents of 
    // the low-order register (C, E, L, flags) are copied to that location.
    // Example − PUSH K
    INSTRUCTION1(PUSH_B, 0xc5)
    INSTRUCTION1(PUSH_D, 0xd5)
    INSTRUCTION1(PUSH_H, 0xe5)
    INSTRUCTION1(PUSH_A, 0xf5)

    // Pop off stack to the register pair
    // The contents of the memory location pointed out by the stack pointer 
    // register are copied to the low-order register (C, E, L, status flags) of
    // the operand.
    // 1. The stack pointer is incremented by 1 and the contents of that memory 
    // location are copied to the high-order register (B, D, H, A) of the 
    // operand.
    // 2. The stack pointer register is again incremented by 1.
    // Example − POP K
    INSTRUCTION1(POP_B, 0xc1)
    INSTRUCTION1(POP_D, 0xd1)
    INSTRUCTION1(POP_H, 0xe1)
    INSTRUCTION1(POP_A, 0xf1)

    // Output the data from the accumulator to a port with 8bit address
    // The contents of the accumulator are copied into the I/O port specified 
    // by the operand.
    // Example − OUT K9L
    INSTRUCTION1(OUT, 0xd3)

    // Input data to accumulator from a port with 8-bit address
    // The contents of the input port designated in the operand are read and 
    // loaded into the accumulator.
    // Example − IN 5KL
    INSTRUCTION1(IN, 0xdb)

#undef INSTRUCTION1
