#include "decoder.h"
#include "common.h"
#include "display.h"
#include "8085.h"
#include "dump.h"

Instruction decode_ins(u8 *ins){
    Instruction i;
    u8 opcode = *ins;
    
    i.reg[0] = i.reg[1] = 7;
    i.operand0 = i.operand1 = 0;

    #define is_between(x, y) \
        if(opcode >= x && opcode <= y)
    #define INSTRUCTION(name, x, y) \
    else is_between(x, y){ \
        i.op = name; \
    }

    if(0){
        err("Not a valid opcode!");
    }
    #include "instruction.h"
    #undef INSTRUCTION

    pgrnh("\n[Decoding] ", "%s", machine_get_opcode_string(i.op));

    u8 preg = 1;

    is_between(0xa0, 0xa7){
        // ANA
        i.reg[0] = opcode & 0x7;
        i.operand0 = i.reg[0] != 6;
    }
    else is_between(0xa8, 0xaf){
        // XRA
        i.reg[0] = (opcode & 0xf) - 8;
        i.operand0 = i.reg[0] != 6;
    }
    else is_between(0xb0, 0xb7){
        // ORA
        i.reg[0] = (opcode & 0x7);
        i.operand0 = i.reg[0] != 6;
    }
    else is_between(0x80, 0x87){
        // ADD 
        i.reg[0] = (opcode & 0x7);
        i.operand0 = i.reg[0] != 6;
    }
    else is_between(0x88, 0x8f){
        // ADC
        i.reg[0] = (opcode & 0xf) - 8;
        i.operand0 = i.reg[0] != 6;
    }
    else is_between(0x90, 0x97){
        // SUB
        i.reg[0] = (opcode & 0x7);
        i.operand0 = i.reg[0] != 6;
    }
    else is_between(0x98, 0x9f){
        // SBB
        i.reg[0] = (opcode & 0xf) - 8;
        i.operand0 = i.reg[0] != 6;
    }
    else is_between(0x40, 0x7f){
        // MOV
        i.reg[1] = (opcode & 0x7);
        i.reg[0] = (opcode >> 3) & 0x7;
        i.operand0 = i.reg[0] != 6;
        i.operand1 = i.reg[1] != 6;
    }
    else
        preg = 0;

    if(preg){
        if(i.reg[0] == 7)
            i.reg[0] = 6;
        if(i.reg[1] == 7)
            i.reg[1] = 6;

        char reg[] = {'B', 'C', 'D', 'E', 'G', 'H', 'A', ' '};
        pblue(" %c", reg[i.reg[0]]);
        i.operand1? pblue(", %c", reg[i.reg[1]]) : pblue(" ");
    }
    return i;
}

static u16 loc = 0;

static u16 mw_addr(u16 addr, u8 data){
    mem_write(addr, data);
    return addr;
}

static u16 mw(u8 data){
    mem_write(loc++, data);
    return loc-1;
}

static u16 mw16(u16 data){
    mw((data & 0xff00) >> 8);
    mw(data & 0x00ff);
    return loc - 2;
}

static u16 mw16_addr(u16 addr, u16 data){
    mw_addr(addr, (data & 0xff00) >> 8);
    mw_addr(addr + 1, data & 0x00ff);
    return addr;
}

int main(){
    dump_init();
    #define INSTRUCTION(name, range0, range1) \
            pblue("\n%8s", #name); \
            pgrn(" : " #range0 " - " #range1); \
            if(range1 < range0) \
                err("Invalid range for " #name "!");
    #include "instruction.h"
    #undef INSTRUCTION
    machine_init();
    //mw(LDI);
    // mw(0);
    mw(0x06); // x = 0
    mw(0);
    mw(0x0e); // y = 1
    mw(1);

    // z = x, A <- B, 01 000 111
    u16 start = mw(0x47);
    // z = z + y, A <- A + C
    mw(0x81);
    // x = y, B <- C, 01 001 000
    mw(0x48);
    // y = z, C <- A, 01 111 001
    mw(0x79);
    // z < 255 ?
    mw(0xfe);
    mw(120);
    // jc
    mw(0xda);
    mw16(start);
    mw(0x76);

    //mw16_addr(add2, loc+1);
    machine_print();
    dbg("Machine started!");
    machine_run();
    dbg("Run completed!");
    machine_print();
    return 0;
}
